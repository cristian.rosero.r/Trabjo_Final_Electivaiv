// CONTROL MOTORES TB6612FNG

#define PWMA 2
#define PWMB 4
#define AIN1 13
#define AIN2 14
#define BIN1 15
#define BIN2 16
#define STBY 17
#define FREQ 1000
#define RESOLUTION 10
#define CHANNEL_MOTOR_IZQ 0
#define CHANNEL_MOTOR_DER 1


// CONTROL SENSOR QTR-8RC

#define QTR8RC_1 32
#define QTR8RC_2 33
#define QTR8RC_3 34
#define QTR8RC_4 35
#define QTR8RC_5 25
#define QTR8RC_6 23
#define QTR8RC_7 19
#define QTR8RC_8 18
#define EMISORLED 5
#define NUM_SENSORS 8      
#define TIMEOUT 2500  

void control_motor_izquierdo(void);
void control_motor_derecho(void); 

void setup() {

  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  pinMode(STBY, OUTPUT);
  digitalWrite(STBY, LOW);

  ledcSetup(CHANNEL_MOTOR_IZQ,FREQ,RESOLUTION);
  ledcSetup(CHANNEL_MOTOR_DER,FREQ,RESOLUTION);
  ledcAttachPin(PWMA, CHANNEL_MOTOR_IZQ);
  ledcAttachPin(PWMB, CHANNEL_MOTOR_DER);

}

void loop() {

  control_motor_izquierdo(1,500);
  delay(2000);
  control_motor_derecho(1,500);
  delay(2000);
  
}

void control_motor_izquierdo(unsigned char sentido, unsigned int revoluciones)
{
  if(sentido == 1) // adelante
  {
    digitalWrite(STBY, HIGH);
    digitalWrite(AIN1, HIGH);
    digitalWrite(AIN2, LOW);
    ledcWrite(CHANNEL_MOTOR_IZQ,revoluciones); 
  }

  else if(sentido == 2) // atras
  {
    digitalWrite(STBY, HIGH);
    digitalWrite(AIN1, LOW);
    digitalWrite(AIN2, HIGH);
    ledcWrite(CHANNEL_MOTOR_IZQ,revoluciones);
  }
  else // detener
  {
    digitalWrite(AIN1, LOW);
    digitalWrite(AIN2, LOW);
    ledcWrite(CHANNEL_MOTOR_IZQ,revoluciones);
  }

}

void control_motor_derecho(unsigned char sentido, unsigned int revoluciones)
{
  if(sentido == 1) // adelante
  {
    digitalWrite(STBY, HIGH);
    digitalWrite(BIN1, HIGH);
    digitalWrite(BIN2, LOW);
    ledcWrite(CHANNEL_MOTOR_DER,revoluciones); 
  }

  else if(sentido == 2) // atras
  {
    digitalWrite(STBY, HIGH);
    digitalWrite(BIN1, LOW);
    digitalWrite(BIN2, HIGH);
    ledcWrite(CHANNEL_MOTOR_DER,revoluciones);
  }
  else
  {
    digitalWrite(BIN1, LOW);
    digitalWrite(BIN2, LOW);
    ledcWrite(CHANNEL_MOTOR_DER,revoluciones);
  }

}
