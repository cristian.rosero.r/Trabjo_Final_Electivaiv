// Librerias

#include <QTRSensors.h>
#include "BluetoothSerial.h"

// CONTROL MOTORES TB6612FNG

#define PWMA 2
#define PWMB 4
#define AIN1 13
#define AIN2 14
#define BIN1 15
#define BIN2 16
#define STBY 17
#define FREQ 1000
#define RESOLUTION 10
#define CHANNEL_MOTOR_IZQ 0
#define CHANNEL_MOTOR_DER 1


// CONTROL SENSOR QTR-8RC

#define QTR8RC_1 32
#define QTR8RC_2 33
#define QTR8RC_3 34
#define QTR8RC_4 35
#define QTR8RC_5 25
#define QTR8RC_6 23
#define QTR8RC_7 22
#define QTR8RC_8 39
#define EMISORLED 5 //calibrar sensores antes de moner en movimiento el carro
#define NUM_SENSORS 8      
#define TIMEOUT 2500 //tiempo

// Variables globales
BluetoothSerial SerialBT;
QTRSensors qtr;
uint16_t sensorValues[NUM_SENSORS];

// Variables de control PID
float constanteI = 0.001;
float constanteD = 0.01;
float constanteP = 0.055;
int velocidad_maxima = 700; // Resolucion profundidad 10 bits para PWM --> 1023 
float proporcional = 0;
float integral = 0;
float derivativa = 0;
float set_point = 3500; // valor inicial ajustado por la lectura del sensor en la barra negra sensores del medio detectan la linea
float error = 0;
int control = 0;
int constanteGiro = 0; // Se debe ajustar con los valores del sensor para izquierda y derecha
int error_pasado=0; //actualizacion
// Funciones
void control_motor_izquierdo(void);
void control_motor_derecho(void); 
void configuracion_pines_SensorQTR(void);
void calibracionSensorQTR(void);
void lectura_sensorQTR(void);

void setup() 
{
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  pinMode(STBY, OUTPUT);
  digitalWrite(STBY, LOW);
  Serial.begin(115200);
  ledcSetup(CHANNEL_MOTOR_IZQ,FREQ,RESOLUTION);
  ledcSetup(CHANNEL_MOTOR_DER,FREQ,RESOLUTION);
  ledcAttachPin(PWMA, CHANNEL_MOTOR_IZQ);
  ledcAttachPin(PWMB, CHANNEL_MOTOR_DER);

  
  qtr.setTypeRC();

  qtr.setSensorPins((const uint8_t[]){QTR8RC_1,QTR8RC_2,QTR8RC_3,QTR8RC_4,QTR8RC_5,QTR8RC_6,QTR8RC_7,QTR8RC_8} ,NUM_SENSORS);

  qtr.setEmitterPin(EMISORLED);
  
  //SerialBT.begin("ESP32_Bluetooth");

  configuracion_pines_SensorQTR();

  calibracionSensorQTR();

}

void loop() 
{
  


    lectura_sensorQTR();
    unsigned int posicion = qtr.readLineBlack(sensorValues);
    
    //SerialBT.print("Sensor: ");
    //SerialBT.println(posicion);
    //delay(250);
    // Calculo del error
    error = set_point- posicion;
    // Calculo del control 
    proporcional = error * constanteP;
    integral = (error_pasado + error) ;
    derivativa = (error - error_pasado);
    control = (int)(proporcional + constanteI*integral +  constanteD*derivativa);
    //set_point = posicion;
    //SerialBT.print("Control: ");
    //SerialBT.println(control);
    // Control de direccion
    if(control > constanteGiro)
    {
      control_motor_izquierdo(2,velocidad_maxima/4);
      control_motor_derecho(1,velocidad_maxima);
    }
    else if (control < constanteGiro)
    {
      control_motor_izquierdo(1,velocidad_maxima);
      control_motor_derecho(2,velocidad_maxima/4);
    }
    else{
      control_motor_izquierdo(1,400);
      control_motor_derecho(1,400);
      }
     error_pasado=error;
}


void configuracion_pines_SensorQTR(void)
{
  
  pinMode(PWMA, OUTPUT);
  pinMode(PWMB, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  pinMode(STBY, OUTPUT);
  
  pinMode(QTR8RC_1 , INPUT);
  pinMode(QTR8RC_2 , INPUT);
  pinMode(QTR8RC_3 , INPUT);
  pinMode(QTR8RC_4 , INPUT);
  pinMode(QTR8RC_5 , INPUT);
  pinMode(QTR8RC_6 , INPUT);
  pinMode(QTR8RC_7 , INPUT);
  pinMode(QTR8RC_8 , INPUT);

}

void control_motor_izquierdo(unsigned char sentido, unsigned int revoluciones)
{
  if(sentido == 1) // adelante
  {
    digitalWrite(STBY, HIGH);
    digitalWrite(AIN1, LOW);
    digitalWrite(AIN2, HIGH);
    ledcWrite(CHANNEL_MOTOR_IZQ,revoluciones); 
  }

  else if(sentido == 2) // atras
  {
    digitalWrite(STBY, HIGH);
    digitalWrite(AIN1, HIGH);
    digitalWrite(AIN2, LOW);
    ledcWrite(CHANNEL_MOTOR_IZQ,revoluciones);
  }
  else // detener
  {
    digitalWrite(AIN1, LOW);
    digitalWrite(AIN2, LOW);
    ledcWrite(CHANNEL_MOTOR_IZQ,revoluciones);
  }

}

void control_motor_derecho(unsigned char sentido, unsigned int revoluciones)
{
  if(sentido == 1) // adelante
  {
    digitalWrite(STBY, HIGH);
    digitalWrite(BIN1, HIGH);
    digitalWrite(BIN2, LOW);
    ledcWrite(CHANNEL_MOTOR_DER,revoluciones); 
  }

  else if(sentido == 2) // atras
  {
    digitalWrite(STBY, HIGH);
    digitalWrite(BIN1, LOW);
    digitalWrite(BIN2, HIGH);
    ledcWrite(CHANNEL_MOTOR_DER,revoluciones);
  }
  else
  {
    digitalWrite(BIN1, LOW);
    digitalWrite(BIN2, LOW);
    ledcWrite(CHANNEL_MOTOR_DER,revoluciones);
  }

}

void calibracionSensorQTR(void)
{

  for(int i = 0; i < 100; i++){
    
    qtr.calibrate();  
  }
  delay(500);
  for (int i = 0; i < NUM_SENSORS; i++){

    SerialBT.write(qtr.calibrationOn.minimum[i]);
    SerialBT.print(' ');
  }
  delay(1000);

  for (int i = 0; i < NUM_SENSORS; i++){

    SerialBT.write(qtr.calibrationOn.maximum[i]);
    SerialBT.print(' ');
  }
  delay(1000);

}

void lectura_sensorQTR(void){
  unsigned int posicion = qtr.readLineBlack(sensorValues);
  for (uint8_t i = 0; i < NUM_SENSORS; i++){
    Serial.print(sensorValues[i]);
    Serial.print('\t');
    }
    Serial.print(posicion);
    Serial.print(" ");
    Serial.println(control);
}
