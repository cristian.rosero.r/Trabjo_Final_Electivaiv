// CONTROL MOTORES TB6612FNG

#define PWMA 2
#define PWMB 4
#define AIN1 13
#define AIN2 14
#define BIN1 15
#define BIN2 16
#define STBY 17

// CONTROL SENSOR QTR-8RC

#define QTR8RC_1 32
#define QTR8RC_2 33
#define QTR8RC_3 34
#define QTR8RC_4 35
#define QTR8RC_5 25
#define QTR8RC_6 33
#define QTR8RC_7 19
#define QTR8RC_8 18


void setup() 
{
  pinMode(PWMA, OUTPUT);
  pinMode(PWMB, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  pinMode(STBY, OUTPUT);
  
  pinMode(QTR8RC_1 , INPUT);
  pinMode(QTR8RC_2 , INPUT);
  pinMode(QTR8RC_3 , INPUT);
  pinMode(QTR8RC_4 , INPUT);
  pinMode(QTR8RC_5 , INPUT);
  pinMode(QTR8RC_6 , INPUT);
  pinMode(QTR8RC_7 , INPUT);
  pinMode(QTR8RC_8 , INPUT);

}

void loop() 
{

}
